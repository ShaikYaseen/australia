package com.cms.cardmanagementsystem.exception;

public class CustomerNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	private final Integer errorCode;

	public CustomerNotFoundException(String message, Integer errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

}
