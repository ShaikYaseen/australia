package com.cms.cardmanagementsystem.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class DebitCardResponse {
	
	@JsonProperty(value = "message")
	private String message;
	@JsonProperty(value = "statusCode")
	private Integer statusCode;

	public DebitCardResponse(String message, Integer statusCode) {
		super();
		this.message = message;
		this.statusCode = statusCode;
	}

}
