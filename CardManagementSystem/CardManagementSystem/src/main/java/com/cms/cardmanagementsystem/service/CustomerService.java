package com.cms.cardmanagementsystem.service;

import com.cms.cardmanagementsystem.dto.DebitCardResponse;
import com.cms.cardmanagementsystem.exception.CustomerNotFoundException;

public interface CustomerService {

	public DebitCardResponse createNewDebitCard(Long customerId) throws CustomerNotFoundException;

}
