package com.cms.cardmanagementsystem.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "debit_card")
@Data
@Getter
@Setter
public class DebitCard implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "debit_card_id")
	private Long debitCardId;

	@Column(name = "cvv")
	private Integer cvv;

	@Column(name = "card_number")
	private Long cardNumber;

	@Column(name = "card_expiry_date")
	private LocalDate cardExpiryDate;

	@Column(name = "card_status")
	private String cardStatus;

	@Column(name = "customer_id")
	private Long customerId;

}
