package com.cms.cardmanagementsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cms.cardmanagementsystem.entity.DebitCard;

@Repository
public interface DebitCardRepository extends JpaRepository<DebitCard, Long> {

}
