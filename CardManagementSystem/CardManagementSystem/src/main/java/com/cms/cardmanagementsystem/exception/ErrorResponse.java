package com.cms.cardmanagementsystem.exception;

import lombok.Data;

@Data
public class ErrorResponse {

	private Integer statusCode;
	
	private String message;
	
}
