package com.cms.cardmanagementsystem.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(CustomerNotFoundException.class)
	public ResponseEntity<Object> customerNotFoundException(CustomerNotFoundException customerNotFoundException) {

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setStatusCode(customerNotFoundException.getErrorCode());
		errorResponse.setMessage(customerNotFoundException.getMessage());
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}
}
