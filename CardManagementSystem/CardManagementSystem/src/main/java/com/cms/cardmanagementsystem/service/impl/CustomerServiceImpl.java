package com.cms.cardmanagementsystem.service.impl;

import java.time.LocalDate;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.cms.cardmanagementsystem.dto.DebitCardResponse;
import com.cms.cardmanagementsystem.entity.Customer;
import com.cms.cardmanagementsystem.entity.DebitCard;
import com.cms.cardmanagementsystem.exception.CustomerNotFoundException;
import com.cms.cardmanagementsystem.repository.CustomerRepository;
import com.cms.cardmanagementsystem.repository.DebitCardRepository;
import com.cms.cardmanagementsystem.service.CustomerService;
import com.cms.cardmanagementsystem.utility.Constants;

@Service
@PropertySource("classpath:messages.properties")
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private DebitCardRepository debitCardRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Value("${com.cms.cardmanagementsystem.customernotfound.statuscode}")
	private Integer customerNotFoundStatusCode;

	@Value("${com.cms.cardmanagementsystem.customernotfound.statusmessage}")
	private String customerNotFoundStatusMessage;

	@Value("${com.cms.cardmanagementsystem.success.statuscode}")
	private Integer successCode;

	@Value("${com.cms.cardmanagementsystem.success.message}")
	private String successMessage;

	@Transactional
	public DebitCardResponse createNewDebitCard(Long customerId) throws CustomerNotFoundException {

		Customer customer = customerRepository.findById(customerId)
				.orElseThrow(() -> new CustomerNotFoundException(
						new StringBuffer(customerNotFoundStatusMessage).append(customerId).toString(),
						customerNotFoundStatusCode));
		Integer maximumCvvNumber = 1000;
		Integer minimumCvvNumber = 100;
		Integer cvvNumber = (int) (Math.random() * (maximumCvvNumber - minimumCvvNumber + 1) + minimumCvvNumber);
		Long maximum = 1000000000000l;
		Long minimum = 99999999999l;
		Long cardNumber = (long) ((Math.random() * (maximum - minimum + 1)) + minimum);
		DebitCard debitCard = new DebitCard();
		debitCard.setCardNumber(cardNumber);
		debitCard.setCustomerId(customer.getCustomerId());
		debitCard.setCvv(cvvNumber);
		debitCard.setCardExpiryDate(LocalDate.now().plusYears(3));
		debitCard.setCardStatus(Constants.ACTIVE);
		debitCardRepository.save(debitCard);

		return new DebitCardResponse(successMessage, successCode);
	}

	
}
