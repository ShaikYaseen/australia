package com.cms.cardmanagementsystem.controller;

import java.time.LocalDate;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.cms.cardmanagementsystem.dto.DebitCardResponse;
import com.cms.cardmanagementsystem.exception.CustomerNotFoundException;
import com.cms.cardmanagementsystem.service.CustomerService;

@RestController
@RequestMapping("/cards")
class CustomerController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private RestTemplate restTemplate;

	@Scheduled(fixedRate = 60000)
	public void sheduled() {
		test();
	}

	@GetMapping("/schedule")
	public void test() {
		System.out.println(LocalDate.now());
	}

	@PostMapping("/{customerId}")
	public ResponseEntity<DebitCardResponse> createNewDebitCard(@PathVariable Long customerId)
			throws CustomerNotFoundException {

		return new ResponseEntity<>(customerService.createNewDebitCard(customerId), HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<Object> getFriends() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		return new ResponseEntity<>(
				restTemplate.exchange("http://localhost:8080/friends", HttpMethod.GET, entity, String.class).getBody(),
				HttpStatus.OK);
	}

	@PostMapping("")
	public ResponseEntity<Object> createFrinds(@RequestBody User user) {

		HttpHeaders headers = new HttpHeaders();

		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<User> entity = new HttpEntity<User>(user, headers);

		return new ResponseEntity<>(restTemplate
				.exchange("http://localhost:8080/friends/update", HttpMethod.POST, entity, String.class).getBody(),
				HttpStatus.OK);
	}

	@DeleteMapping("")
	public ResponseEntity<Object> updateFrinds(@RequestBody User user, Long userId) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<User> entity = new HttpEntity<User>(user, headers);
		return new ResponseEntity<>(restTemplate
				.exchange("http://localhost:8080/friends/update" + userId, HttpMethod.DELETE, entity, String.class)
				.getBody(), HttpStatus.OK);
	}

}
