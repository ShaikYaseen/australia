package com.eureka.server.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RequestMapping(path = "/movies")
@RestController
public class MovieCatalogController {

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping
	public ResponseEntity<Object> getFriends() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		return new ResponseEntity<>(
				restTemplate.exchange("http://localhost:8080/friends", HttpMethod.GET, entity, String.class).getBody(),
				HttpStatus.OK);
	}
}
