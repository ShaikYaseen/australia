package com.demo.practice.scheduleconfig;

public class CaronaThreat {

	private String state;

	private String city;

	private Integer male;

	private Integer female;

	public CaronaThreat(String state, String city, Integer male, Integer female) {
		super();
		this.state = state;
		this.city = city;
		this.male = male;
		this.female = female;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getMale() {
		return male;
	}

	public void setMale(Integer male) {
		this.male = male;
	}

	public Integer getFemale() {
		return female;
	}

	public void setFemale(Integer female) {
		this.female = female;
	}

	@Override
	public String toString() {
		return "CaronaThreat [state=" + state + ", city=" + city + ", male=" + male + ", female=" + female + "]";
	}

}
