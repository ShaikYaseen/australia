package com.demo.practice.scheduleconfig;

public class StateCount {

	private String state;

	private Integer totalCases;

	public StateCount(String state, Integer totalCases) {
		super();
		this.state = state;
		this.totalCases = totalCases;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getTotalCases() {
		return totalCases;
	}

	public void setTotalCases(Integer totalCases) {
		this.totalCases = totalCases;
	}

}
