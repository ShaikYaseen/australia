package com.demo.practice.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/term")
public class ScheduledTerm {

	@Scheduled(fixedRate = 60000)
	public void sheduled() throws InterruptedException {
		test();
	}

	// @Scheduled(cron = "0 * 7 * * ?")
	@GetMapping
	public void test() throws InterruptedException {

		for (int i = 0; i <= 10; i++) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date now = new Date();
			String strDate = sdf.format(now);
			System.out.println("Java cron job expression:: " + strDate);
			Thread.sleep(2000);
		}

	}
}
