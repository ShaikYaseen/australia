package com.fmdemo.friendssuggestion.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.PropertySource;

import com.fmdemo.friendssuggestion.controller.TestData;
import com.fmdemo.friendssuggestion.dto.FriendDTO;
import com.fmdemo.friendssuggestion.dto.FriendsRequest;
import com.fmdemo.friendssuggestion.dto.UserDTO;
import com.fmdemo.friendssuggestion.dto.UserNameDTO;
import com.fmdemo.friendssuggestion.entity.Friend;
import com.fmdemo.friendssuggestion.entity.User;
import com.fmdemo.friendssuggestion.exception.FriendRequestNotFoundException;
import com.fmdemo.friendssuggestion.exception.UserAlreadyExcistException;
import com.fmdemo.friendssuggestion.exception.UserNotFoundException;
import com.fmdemo.friendssuggestion.repository.FriendRepository;
import com.fmdemo.friendssuggestion.repository.UserRepository;
import com.fmdemo.friendssuggestion.response.FriendResponse;
import com.fmdemo.friendssuggestion.response.UserResponse;
import com.fmdemo.friendssuggestion.service.api.UserService;

@SpringBootTest
@PropertySource("classpath:messages.properties")
public class UserServiceTest {

	@Autowired
	private UserService userService;

	@MockBean
	private UserRepository userRepository;

	@MockBean
	private FriendRepository friendRepository;
	@Value("${com.fmdemo.friendssuggestion.exception.usernotfound.errormessage}")
	private String userNotFooundErrorMessage;

	@Value("${com.fmdemo.friendssuggestion.exception.usernotfound.errorcode}")
	private Integer userNotFooundErrorCode;

	User user, user1;
	Friend friend;
	UserDTO userDto;

	FriendsRequest friendRequest;

	List<UserNameDTO> userNameDTOs;
	FriendDTO friendDto;
	UserNameDTO userNameDto, userNameDto1;

	List<User> userNames;

	@BeforeEach
	public void setUp() {
		user = new User();
		user.setUserName(TestData.userName);
		user.setFullName(TestData.fullName);
		user1 = new User();
		user1.setUserName(TestData.userName1);
		user1.setFullName(TestData.fullName);

		userDto = new UserDTO();
		userDto.setUserName(TestData.userName);
		userDto.setFullName(TestData.fullName);

		userNameDto1 = new UserNameDTO();
		userNameDto1.setUserName(TestData.userName1);

		friend = new Friend();
		friend.setUserId(user.getUserId());

		friendDto = new FriendDTO();
		friendDto.setUserName(TestData.userName);

		userNameDTOs = new ArrayList<>();
		userNameDTOs.add(userNameDto1);

		userNames = new ArrayList<>();
		userNames.add(user);
		userNames.add(user1);

		friendRequest = new FriendsRequest();
		friendRequest.setUserName(TestData.userName);
		friendRequest.setUserNames(userNameDTOs);
	}

	@Test
	public void addMemberPositiveTest() throws UserAlreadyExcistException {

		when(userRepository.findById(TestData.userId)).thenReturn(Optional.empty());
		UserResponse response = new UserResponse("Member added successfully ", 7676);
		UserResponse userResponse = userService.addMember(userDto);
		assertThat(userResponse.getMessage()).isEqualTo(response.getMessage());
		assertThat(userResponse.getStatusCode()).isEqualTo(response.getStatusCode());
	}

	@Test
	public void addFriendsTest() throws UserNotFoundException, FriendRequestNotFoundException {

		List<User> friends = new ArrayList<>();
		when(userRepository.findByUserName(friendRequest.getUserName())).thenReturn(Optional.of(user));
		for (UserNameDTO friend : friendRequest.getUserNames()) {

			friends.add(getUser(friend.getUserName()));
		}
		for (User users : friends) {
			Friend friendReq = new Friend();
			friend.setUserId(user.getUserId());
			friend.setFriendId(users.getUserId());
			when(friendRepository.save(friendReq)).thenReturn(friendReq);
		}
		FriendResponse friendResponse = new FriendResponse("Friends saved Successfully", 6878);
		FriendResponse response = userService.addFriends(friendRequest);
		assertThat(friendResponse.getMessage()).isEqualTo(response.getMessage());
		assertThat(friendResponse.getStatusCode()).isEqualTo(response.getStatusCode());

	}

	@Test
	public void addFriendsThrowsUserNotFoundTest() throws UserNotFoundException, FriendRequestNotFoundException {
		FriendsRequest request = new FriendsRequest();
		request.setUserName(TestData.userName);
		request.setUserNames(null);
		when(userRepository.findByUserName(friendRequest.getUserName())).thenReturn(Optional.of(user));

		assertThrows(UserNotFoundException.class, () -> userService.addFriends(request));
	}

	@Test
	public void addMemberThrowsUserAlreadyExcistTest() throws UserNotFoundException, FriendRequestNotFoundException {

		when(userRepository.findByUserName(friendRequest.getUserName())).thenReturn(Optional.of(user));

		assertThrows(UserAlreadyExcistException.class, () -> userService.addMember(userDto));

	}

	public User getUser(String userName) throws UserNotFoundException {
		when(userRepository.findByUserName(userName)).thenReturn(Optional.of(user));
		return user;
	}

}
