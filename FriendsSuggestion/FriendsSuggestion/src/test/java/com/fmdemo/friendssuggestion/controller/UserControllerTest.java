/*
 * package com.fmdemo.friendssuggestion.controller;
 * 
 * import static org.assertj.core.api.Assertions.assertThat; import static
 * org.mockito.Mockito.when;
 * 
 * import java.util.ArrayList; import java.util.List;
 * 
 * import org.junit.jupiter.api.BeforeEach; import org.junit.jupiter.api.Test;
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.boot.test.context.SpringBootTest; import
 * org.springframework.boot.test.mock.mockito.MockBean; import
 * org.springframework.http.ResponseEntity;
 * 
 * import com.fmdemo.friendssuggestion.dto.FriendDTO; import
 * com.fmdemo.friendssuggestion.dto.FriendsRequest; import
 * com.fmdemo.friendssuggestion.dto.UserDTO; import
 * com.fmdemo.friendssuggestion.dto.UserNameDTO; import
 * com.fmdemo.friendssuggestion.entity.Friend; import
 * com.fmdemo.friendssuggestion.entity.User; import
 * com.fmdemo.friendssuggestion.exception.FriendRequestNotFoundException; import
 * com.fmdemo.friendssuggestion.exception.UserAlreadyExcistException; import
 * com.fmdemo.friendssuggestion.exception.UserNotFoundException; import
 * com.fmdemo.friendssuggestion.response.FriendResponse; import
 * com.fmdemo.friendssuggestion.response.UserResponse; import
 * com.fmdemo.friendssuggestion.service.api.UserService;
 * 
 * @SpringBootTest public class UserControllerTest {
 * 
 * @Autowired private UserController userController;
 * 
 * @MockBean private UserService userService;
 * 
 * User user, user1; Friend friend; UserDTO userDto;
 * 
 * FriendsRequest friendRequest;
 * 
 * List<UserNameDTO> userNameDTOs; FriendDTO friendDto; UserNameDTO userNameDto,
 * userNameDto1;
 * 
 * List<User> userNames;
 * 
 * @BeforeEach public void setUp() { user = new User();
 * user.setUserName(TestData.userName); user.setFullName(TestData.fullName);
 * user1 = new User(); user1.setUserName(TestData.userName1);
 * user1.setFullName(TestData.fullName);
 * 
 * userDto = new UserDTO(); userDto.setFullName(TestData.fullName);
 * 
 * userNameDto1 = new UserNameDTO();
 * userNameDto1.setUserName(TestData.userName1);
 * 
 * friend = new Friend(); friend.setUserId(user.getUserId());
 * 
 * friendDto = new FriendDTO(); friendDto.setUserName(TestData.userName);
 * 
 * userNameDTOs = new ArrayList<>(); userNameDTOs.add(userNameDto1);
 * 
 * userNames = new ArrayList<>(); userNames.add(user); userNames.add(user1);
 * 
 * friendRequest = new FriendsRequest();
 * friendRequest.setUserName(TestData.userName);
 * friendRequest.setUserNames(userNameDTOs); }
 * 
 * @Test public void addMemberPositiveTest() throws UserAlreadyExcistException {
 * 
 * UserResponse response = new UserResponse("Member added successfully ", 7676);
 * when(userService.addMember(userDto)).thenReturn(response);
 * ResponseEntity<UserResponse> userResponseEntity =
 * userController.addMember(userDto); UserResponse userResponse =
 * userService.addMember(userDto);
 * assertThat(userResponse.getMessage()).isEqualTo(userResponseEntity.getBody().
 * getMessage());
 * assertThat(userResponse.getStatusCode()).isEqualTo(userResponseEntity.getBody
 * ().getStatusCode()); }
 * 
 * @Test public void addFrendsTest() throws FriendRequestNotFoundException,
 * UserNotFoundException { FriendResponse response = new
 * FriendResponse("Friends saved Successfully", 6878);
 * when(userService.addFriends(friendRequest)).thenReturn(response);
 * FriendResponse friendResponse = userService.addFriends(friendRequest);
 * ResponseEntity<FriendResponse> responseEntity =
 * userController.addFriends(friendRequest);
 * assertThat(friendResponse.getMessage()).isEqualTo(responseEntity.getBody().
 * getMessage());
 * assertThat(friendResponse.getStatusCode()).isEqualTo(responseEntity.getBody()
 * .getStatusCode());
 * 
 * }
 * 
 * }
 */