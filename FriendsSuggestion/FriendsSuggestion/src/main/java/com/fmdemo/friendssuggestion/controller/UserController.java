package com.fmdemo.friendssuggestion.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

	// Save the uploaded file to this folder
	private static String UPLOADED_FOLDER = "F://temp//";

	@GetMapping("/")
	public String index() {
		return "upload";
	}

	@PostMapping("/upload") // //new annotation since 4.3
	public String singleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {

		if (file.isEmpty()) {
			redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
			return "redirect:uploadStatus";
		}

		try {

			// Get the file and save it somewhere
			byte[] bytes = file.getBytes();
			Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
			Files.write(path, bytes);

			redirectAttributes.addFlashAttribute("message",
					"You successfully uploaded '" + file.getOriginalFilename() + "'");

		} catch (IOException e) {
			e.printStackTrace();
		}

		return "redirect:/uploadStatus";
	}

	@GetMapping("/uploadStatus")
	public String uploadStatus() {
		return "uploadStatus";
	}

	/*
	 * @Autowired private UserService userService;
	 * 
	 * private static Logger logger = LoggerFactory.getLogger(UserController.class);
	 * 
	 * @GetMapping("") public List<User> getUsers() {
	 * 
	 * List<User> users = new ArrayList<>(); users.add(new User("yaseen"));
	 * users.add(new User("yaseen Shaik")); return users; }
	 * 
	 * @PostMapping("") public ResponseEntity<UserResponse>
	 * addMember(@Valid @RequestBody UserDTO userDTO) throws
	 * UserAlreadyExcistException {
	 * 
	 * logger.info("enters into add member method to save the member");
	 * 
	 * return new ResponseEntity<>(userService.addMember(userDTO),
	 * HttpStatus.CREATED);
	 * 
	 * }
	 * 
	 * @PostMapping("/friends") public ResponseEntity<FriendResponse>
	 * addFriends(@Valid @RequestBody FriendsRequest friendsRequest) throws
	 * FriendRequestNotFoundException, UserNotFoundException {
	 * logger.info("enters into add friends method to save the friends ");
	 * 
	 * return new ResponseEntity<>(userService.addFriends(friendsRequest),
	 * HttpStatus.OK); }
	 * 
	 * @GetMapping("/fr") public ResponseEntity<SuggestionsResponse>
	 * getSugestFriendsByUserName(
	 * 
	 * @RequestParam(required = true) String userName) throws UserNotFoundException
	 * {
	 * 
	 * return new
	 * ResponseEntity<>(userService.getSugestFriendsByUserName(userName.trim()),
	 * HttpStatus.OK);
	 * 
	 * }
	 */
}
