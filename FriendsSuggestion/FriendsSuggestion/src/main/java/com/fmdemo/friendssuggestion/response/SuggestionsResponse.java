package com.fmdemo.friendssuggestion.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fmdemo.friendssuggestion.entity.User;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class SuggestionsResponse {

	@JsonProperty(value = "friends")
	private List<User> friends;

	@JsonProperty(value = "size")
	private Integer size;

	public SuggestionsResponse(List<User> friends, Integer size) {
		super();
		this.friends = friends;
		this.size = size;
	}
}
