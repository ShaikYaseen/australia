package com.fmdemo.friendssuggestion.exception;

public class FriendRequestNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	private final Integer errorCode;

	public FriendRequestNotFoundException(String message, Integer errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public Integer getErrorCode() {
		return errorCode;
	}
}
