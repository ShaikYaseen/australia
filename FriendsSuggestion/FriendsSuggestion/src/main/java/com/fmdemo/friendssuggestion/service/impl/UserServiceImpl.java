package com.fmdemo.friendssuggestion.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fmdemo.friendssuggestion.dto.FriendsRequest;
import com.fmdemo.friendssuggestion.dto.UserDTO;
import com.fmdemo.friendssuggestion.dto.UserNameDTO;
import com.fmdemo.friendssuggestion.entity.Friend;
import com.fmdemo.friendssuggestion.entity.User;
import com.fmdemo.friendssuggestion.exception.FriendRequestNotFoundException;
import com.fmdemo.friendssuggestion.exception.UserAlreadyExcistException;
import com.fmdemo.friendssuggestion.exception.UserNotFoundException;
import com.fmdemo.friendssuggestion.repository.FriendRepository;
import com.fmdemo.friendssuggestion.repository.UserRepository;
import com.fmdemo.friendssuggestion.response.FriendResponse;
import com.fmdemo.friendssuggestion.response.SuggestionsResponse;
import com.fmdemo.friendssuggestion.response.UserResponse;
import com.fmdemo.friendssuggestion.service.api.UserService;

@Service
@PropertySource("classpath:messages.properties")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private FriendRepository friendRepository;

	private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Value("${com.fmdemo.friendssuggestion.exception.usernotfound.errormessage}")
	private String userNotFooundErrorMessage;

	@Value("${com.fmdemo.friendssuggestion.exception.usernotfound.errorcode}")
	private Integer userNotFooundErrorCode;

	@Value("${com.fmdemo.friendssuggestion.exception.useralredyexcist.errorcode}")
	private Integer userAlreadyExcistErrorCode;

	@Value("${com.fmdemo.friendssuggestion.exception.useralredyexcist.errormessage}")
	private String userAlreadyExcistErrorMessage;

	@Value("${com.fmdemo.friendssuggestion.exception.successmessage}")
	private String errorMessage;

	@Value("${com.fmdemo.friendssuggestion.exception.successcode}")
	private String errorCode;

	@Value("${com.fmdemo.friendssuggestion.exception.friendrequestrnotfound.errorcode}")
	private Integer friendRequestErrorCode;

	@Value("${com.fmdemo.friendssuggestion.exception.friendrequestrnotfound.errormessage}")
	private String friendRequestErrorMessage;

	@Transactional
	public UserResponse addMember(UserDTO userDTO) throws UserAlreadyExcistException {
		logger.info("save the member");
		Optional<User> user = userRepository.findByUserName(userDTO.getUserName());

		if (user.isPresent()) {
			logger.warn("user is already exist ");
			throw new UserAlreadyExcistException(
					new StringBuffer(userAlreadyExcistErrorMessage).append(userDTO.getUserName()).toString(),
					userAlreadyExcistErrorCode);
		}
		User userDetails = new User();

		BeanUtils.copyProperties(userDTO, userDetails);
		
		userRepository.save(userDetails);

		logger.info("member added successfully");

		return new UserResponse("Member added successfully ", 7676);
	}

	@Transactional
	public FriendResponse addFriends(FriendsRequest friendsRequest)
			throws FriendRequestNotFoundException, UserNotFoundException {

		User user = getUser(friendsRequest.getUserName());

		List<User> friends = new ArrayList<>();

		if (CollectionUtils.isEmpty(friendsRequest.getUserNames())) {
			throw new UserNotFoundException(userNotFooundErrorMessage, userNotFooundErrorCode);
		}
		for (UserNameDTO userName : friendsRequest.getUserNames()) {
			friends.add(getUser(userName.getUserName()));
		}

		for (User users : friends) {
			Friend friend = new Friend();
			friend.setUserId(user.getUserId());
			friend.setFriendId(users.getUserId());
			friendRepository.save(friend);
		}
		logger.info("friends added successfully");

		return new FriendResponse("Friends saved Successfully", 6878);
	}

	public User getUser(String userName) throws UserNotFoundException {

		User user = userRepository.findByUserName(userName)
				.orElseThrow(() -> new UserNotFoundException(
						new StringBuffer(userNotFooundErrorMessage).append(userName).toString(),
						userNotFooundErrorCode));
		return user;
	}

	public SuggestionsResponse getSugestFriendsByUserName(String userName) throws UserNotFoundException {
		/*
		 * User user = getUser(userName);
		 * 
		 * Set<Friend> friends = friendRepository.findByUserId(user.getUserId());
		 * 
		 * 
		 * if (CollectionUtils.isEmpty(friends)) { logger.warn("user doesn't exist ");
		 * throw new UserNotFoundException(new
		 * StringBuffer(userNotFooundErrorMessage).append(userName).toString(),
		 * userNotFooundErrorCode); }
		 * 
		 * 
		 * System.out.println(friends);
		 */
		return new SuggestionsResponse(null, 1);
	}

}
