package com.fmdemo.friendssuggestion.dto;

import lombok.Data;

@Data
public class UserNameDTO {

	private String userName;

}
