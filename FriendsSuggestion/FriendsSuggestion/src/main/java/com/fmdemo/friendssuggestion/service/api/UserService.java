package com.fmdemo.friendssuggestion.service.api;

import com.fmdemo.friendssuggestion.dto.FriendsRequest;
import com.fmdemo.friendssuggestion.dto.UserDTO;
import com.fmdemo.friendssuggestion.exception.FriendRequestNotFoundException;
import com.fmdemo.friendssuggestion.exception.UserAlreadyExcistException;
import com.fmdemo.friendssuggestion.exception.UserNotFoundException;
import com.fmdemo.friendssuggestion.response.FriendResponse;
import com.fmdemo.friendssuggestion.response.SuggestionsResponse;
import com.fmdemo.friendssuggestion.response.UserResponse;

public interface UserService {

	UserResponse addMember(UserDTO userDTO) throws UserAlreadyExcistException;

	FriendResponse addFriends(FriendsRequest friendsRequest) throws FriendRequestNotFoundException, UserNotFoundException;

	SuggestionsResponse getSugestFriendsByUserName(String userName) throws UserNotFoundException;

}
