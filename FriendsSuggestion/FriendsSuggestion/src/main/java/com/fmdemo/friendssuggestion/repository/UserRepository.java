package com.fmdemo.friendssuggestion.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fmdemo.friendssuggestion.entity.Friend;
import com.fmdemo.friendssuggestion.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByUserName(String userName);

	List<Friend> findByUserId(User user);

}
