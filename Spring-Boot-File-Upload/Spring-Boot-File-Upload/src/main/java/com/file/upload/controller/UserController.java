package com.file.upload.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.file.upload.entity.Document;
import com.file.upload.repository.DocumentRepository;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private DocumentRepository documentDao;

	@PostMapping("/upload")
	public ResponseEntity<Object> uploadToDB(@RequestParam("file") MultipartFile file) {
		Document doc = new Document();
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		doc.setDocName(fileName);
		try {
			doc.setFile(file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		documentDao.save(doc);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/files/download/")
				.path(fileName).path("/db").toUriString();
		return ResponseEntity.ok(fileDownloadUri);
	}

	@GetMapping("/download/{fileName:.+}/db")
	public ResponseEntity<Object> downloadFromDB(@PathVariable String fileName) {
		Document document = documentDao.findByDocName(fileName);
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(fileName))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
				.body(document.getFile());
	}
}
