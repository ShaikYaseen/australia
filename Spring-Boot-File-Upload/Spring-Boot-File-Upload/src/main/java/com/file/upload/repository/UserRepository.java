package com.file.upload.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.file.upload.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
