package com.asynch.demo.response;

import java.util.List;

import com.asynch.demo.entity.User;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class UserResponse {

	private List<User> user1;

	private List<User> user2;

	private List<User> user3;

}
