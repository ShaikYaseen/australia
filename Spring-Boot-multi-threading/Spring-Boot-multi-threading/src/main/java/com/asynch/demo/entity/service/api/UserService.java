package com.asynch.demo.entity.service.api;

import java.util.List;

import com.asynch.demo.dto.UserDto;
import com.asynch.demo.entity.User;

public interface UserService {

	Object saveUser(UserDto userDto);

	List<User> getUsers();

}
