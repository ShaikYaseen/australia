package com.asynch.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asynch.demo.dto.UserDto;
import com.asynch.demo.entity.User;
import com.asynch.demo.entity.service.api.UserService;
import com.asynch.demo.response.UserResponse;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping
	public ResponseEntity<Object> saveUser(@RequestBody UserDto userDto) {
		return new ResponseEntity<>(userService.saveUser(userDto), HttpStatus.CREATED);
	}

	@GetMapping
	public ResponseEntity<List<User>> getUsers() {
		return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);

	}

	@GetMapping("/asy")
	public ResponseEntity<UserResponse> getAsynchronous() {

		List<User> users1 = userService.getUsers();
		List<User> users2 = userService.getUsers();
		List<User> users3 = userService.getUsers();
		UserResponse userResponse = new UserResponse();
		userResponse.setUser1(users1);
		userResponse.setUser2(users2);
		userResponse.setUser3(users3);
		return new ResponseEntity<UserResponse>(userResponse, HttpStatus.OK);

	}

}
