package com.asynch.demo.entity.service.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.asynch.demo.dto.UserDto;
import com.asynch.demo.entity.User;
import com.asynch.demo.entity.service.api.UserService;
import com.asynch.demo.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Async
	public Object saveUser(UserDto userDto) {
		logger.info("user ending  time: " + new Date());
		User user = new User();
		BeanUtils.copyProperties(userDto, user);
		logger.info("saving user Thread : " + Thread.currentThread().getName());
		logger.info("user ending  time: " + new Date());
		return userRepository.save(user);
	}
	
	@Async
	public List<User> getUsers() {
		logger.info("user ending  time: " + new Date());
		logger.info("get users Thread : " + Thread.currentThread().getName());
		logger.info("user ending  time: " + new Date());
		return userRepository.findAll();
	}

}
