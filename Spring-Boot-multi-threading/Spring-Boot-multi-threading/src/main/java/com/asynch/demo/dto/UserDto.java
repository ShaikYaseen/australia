package com.asynch.demo.dto;

import lombok.Data;

@Data
public class UserDto {

	private String userName;

	private Integer age;
}
