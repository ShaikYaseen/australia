package com.asynch.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMultiThreadingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMultiThreadingApplication.class, args);
	}

}
