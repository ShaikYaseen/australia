package com.hcl.bank.service;

import org.springframework.stereotype.Service;

import com.hcl.bank.dto.FundTransferdto;
import com.hcl.bank.response.FundTransferResponse;
@Service
public interface FundService {

	FundTransferResponse fundTransfer(FundTransferdto fundTransferdto);

}
