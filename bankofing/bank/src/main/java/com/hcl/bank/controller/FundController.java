package com.hcl.bank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hcl.bank.dto.FundTransferdto;
import com.hcl.bank.response.FundTransferResponse;
import com.hcl.bank.response.TransactionResponse;
import com.hcl.bank.service.FundService;
import com.hcl.bank.service.TransactionService;

@RestController
@RequestMapping("/transactions")
public class FundController {

	@Autowired
	private FundService fundService;

	@Autowired
	private TransactionService transactionService;

	/**
	 * fund trasfer
	 * @param fundTransferdto
	 * @return transaction successful message
	 */
	@PostMapping("")
	public ResponseEntity<FundTransferResponse> fundTransfer(@RequestBody FundTransferdto fundTransferdto) {

		FundTransferResponse fundTransferResponse = fundService.fundTransfer(fundTransferdto);

		return new ResponseEntity<>(fundTransferResponse, HttpStatus.OK);

	}

	/**
	 * 
	 * @param accountId
	 * @return transactions list
	 */
	@GetMapping("/{accountId}")
	public ResponseEntity<TransactionResponse> getTransactionsStatement(@PathVariable("accountId") Long accountId) {

		TransactionResponse transactionResponse = transactionService.getTransactionsStatement(accountId);

		return new ResponseEntity<>(transactionResponse, HttpStatus.OK);

	}

}
