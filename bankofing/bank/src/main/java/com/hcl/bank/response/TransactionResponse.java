package com.hcl.bank.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hcl.bank.dto.Transactiondto;

public class TransactionResponse {

	@JsonProperty("sizeOfList")
	private Integer sizeOfList;

	@JsonProperty("fundTransfer")
	private List<Transactiondto> fundTransfer;

	@JsonProperty("statusCode")
	private Integer statusCode = 770;

	@JsonProperty("statusCode")
	public Integer getStatusCode() {
		return statusCode;
	}

	@JsonProperty("statusCode")
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	@JsonProperty("sizeOfList")
	public Integer getSizeOfList() {
		return sizeOfList;
	}

	@JsonProperty("sizeOfList")
	public void setSizeOfList(Integer sizeOfList) {
		this.sizeOfList = sizeOfList;
	}

	@JsonProperty("fundTransfer")
	public List<Transactiondto> getFundTransfer() {
		return fundTransfer;
	}

	@JsonProperty("fundTransfer")
	public void setFundTransfer(List<Transactiondto> fundTransfer) {
		this.fundTransfer = fundTransfer;
	}

}
