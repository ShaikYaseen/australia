package com.hcl.bank.service;

import org.springframework.stereotype.Service;

import com.hcl.bank.response.TransactionResponse;
@Service
public interface TransactionService {

	TransactionResponse getTransactionsStatement(Long accountId);

}
