package com.hcl.bank.response;

import java.util.List;

public class TransactionResponse {

	private List<UserTransaction>  userTransactions;

	private Integer sizeOfList;

	

	public List<UserTransaction> getUserTransactions() {
		return userTransactions;
	}

	public void setUserTransactions(List<UserTransaction> userTransactions) {
		this.userTransactions = userTransactions;
	}

	public Integer getSizeOfList() {
		return sizeOfList;
	}

	public void setSizeOfList(Integer sizeOfList) {
		this.sizeOfList = sizeOfList;
	}

}
