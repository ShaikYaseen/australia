package com.hcl.bank.exception;

public class ToAccoountNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ToAccoountNotFoundException(String message) {
		super(message);
	}
	
	

}
