package com.hcl.bank.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bank.dto.UserDTO;
import com.hcl.bank.exception.InvalidAccountException;
import com.hcl.bank.exception.InvalidUserException;
import com.hcl.bank.exception.UserNotFoundExcption;
import com.hcl.bank.model.Account;
import com.hcl.bank.model.User;
import com.hcl.bank.repository.AccountRepository;
import com.hcl.bank.repository.UserRepository;
import com.hcl.bank.response.UserAccount;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AccountRepository accountRepository;

	@Transactional
	public User saveUser(UserDTO userDTO) throws InvalidAccountException, InvalidUserException {

		User user = new User();
		Account account = new Account();
		User dbUser = userRepository.findByEmail(userDTO.getEmail());
		List<Account> accounts = new ArrayList<Account>();
		Integer maximum = 1000000;
		Integer minimum = 99999;

		Long accountNumber = (long) ((Math.random() * (maximum - minimum + 1)) + minimum);

		if (dbUser == null) {

			user.setAddress(userDTO.getAddress());
			user.setEmail(userDTO.getEmail());
			user.setUserName(userDTO.getUserName());
			user.setPassword(userDTO.getPassword());

			account.setAccountNumber(accountNumber);
			account.setBalanceAmount(userDTO.getBalanceAmount());
			account.setAccountType(userDTO.getAccountType());

			accounts.add(account);
			userRepository.save(user);

			account.setUserId(user);
			accountRepository.save(account);
			return user;
		} else {
			Account dbAccount = accountRepository.findByAccountType(userDTO.getAccountType());

			User accountUserId = dbAccount.getUserId();
			Long userId = dbUser.getUserId();
			User checkUser = userRepository.findById(userId).get();

			if (!accountUserId.equals(checkUser)) {

				account.setAccountNumber(accountNumber);
				account.setBalanceAmount(userDTO.getBalanceAmount());
				account.setAccountType(userDTO.getAccountType());
				account.setUserId(dbUser);
				accountRepository.save(account);
				return user;
			}

			throw new InvalidUserException("user is already exist");
		}

	}

	public List<UserAccount> getUserAccounts(Long userId) throws UserNotFoundExcption {

		Optional<User> user = userRepository.findById(userId);

		if (!user.isPresent()) {
			throw new UserNotFoundExcption("user not found exception");
		}

		List<Account> account = user.get().getAccounts();

		List<UserAccount> userAccounts = account.stream().map(accounts -> {
			UserAccount userAccount = new UserAccount();

			BeanUtils.copyProperties(accounts, userAccount);
			return userAccount;
		}).collect(Collectors.toList());

		return userAccounts;
	}

	@Transactional
	public String deleteUser(Long userId) throws UserNotFoundExcption {

		Optional<User> user = userRepository.findById(userId);
		if (!user.isPresent()) {
			throw new UserNotFoundExcption("user not found exception");
		}
		userRepository.deleteById(user.get().getUserId());

		return "successfully deleted";
	}

}
