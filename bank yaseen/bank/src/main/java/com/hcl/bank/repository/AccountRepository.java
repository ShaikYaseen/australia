package com.hcl.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.bank.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

	Account findByAccountType(String accountType);

	Account findByAccountNumber(Long fromAccount);

}
