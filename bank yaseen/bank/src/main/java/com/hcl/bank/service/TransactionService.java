package com.hcl.bank.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.hcl.bank.dto.TransactionDTO;
import com.hcl.bank.exception.FromAccountNotFoundException;
import com.hcl.bank.exception.InsufficientFundException;
import com.hcl.bank.exception.ToAccoountNotFoundException;
import com.hcl.bank.exception.TransactionNotFoundException;
import com.hcl.bank.exception.UserNotFoundExcption;
import com.hcl.bank.model.Account;
import com.hcl.bank.model.Transaction;
import com.hcl.bank.model.User;
import com.hcl.bank.repository.AccountRepository;
import com.hcl.bank.repository.TransactionRepository;
import com.hcl.bank.repository.UserRepository;
import com.hcl.bank.response.TransactionResponse;
import com.hcl.bank.response.UserTransaction;

@Service
public class TransactionService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private TransactionRepository transactionRepository;

	@Transactional
	public String fundTransfer(@Valid TransactionDTO transactionDTO)
			throws FromAccountNotFoundException, ToAccoountNotFoundException, InsufficientFundException {

		Account fromAccount = accountRepository.findByAccountNumber(transactionDTO.getFromAccount());
		if (fromAccount == null) {
			throw new FromAccountNotFoundException("From user not found Exception");
		}
		Account toAccount = accountRepository.findByAccountNumber(transactionDTO.getToAccount());
		if (toAccount == null) {
			throw new ToAccoountNotFoundException("To account not found exception");
		}
		
		if(fromAccount.getBalanceAmount() < transactionDTO.getBalanceAmount()) {
			throw new InsufficientFundException("Insufficient fund exception");
		}

		fromAccount.setBalanceAmount(fromAccount.getBalanceAmount() - transactionDTO.getBalanceAmount());

		toAccount.setBalanceAmount(toAccount.getBalanceAmount() + transactionDTO.getBalanceAmount());

		accountRepository.save(fromAccount);
		accountRepository.save(toAccount);

		Transaction fromAccountTransaction = new Transaction();

		fromAccountTransaction.setBalanceAmount(fromAccount.getBalanceAmount());
		fromAccountTransaction.setDebit(transactionDTO.getBalanceAmount());
		fromAccountTransaction.setDateOfTransaction(new Date());
		fromAccountTransaction.setDescription(transactionDTO.getDescription());
		fromAccountTransaction.setFromAccount(fromAccount.getAccountNumber());
		fromAccountTransaction.setToAccount(toAccount.getAccountNumber());

		transactionRepository.save(fromAccountTransaction);

		Transaction toAccountTransaction = new Transaction();

		toAccountTransaction.setBalanceAmount(toAccount.getBalanceAmount());
		toAccountTransaction.setCredit(transactionDTO.getBalanceAmount());
		toAccountTransaction.setDateOfTransaction(new Date());
		toAccountTransaction.setDescription(transactionDTO.getDescription());
		toAccountTransaction.setFromAccount(toAccount.getAccountNumber());
		toAccountTransaction.setToAccount(fromAccount.getAccountNumber());

		transactionRepository.save(toAccountTransaction);

		return "transaction Successful";
	}

	public TransactionResponse transactionStatement(Long userId, Integer pageNumber, Integer pageSize)
			throws UserNotFoundExcption, TransactionNotFoundException {

		Optional<User> user = userRepository.findById(userId);
		if (!user.isPresent()) {
			throw new UserNotFoundExcption("user not found exception");
		}

		PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.Direction.ASC, "transactionId");
		List<Transaction> transactions = transactionRepository.findAll(pageRequest).getContent();

		if (transactions.isEmpty()) {
			throw new TransactionNotFoundException("Transaction not fouond exception");
		}

		List<UserTransaction> userTransaction = transactions.stream().map(transaction -> {
			UserTransaction response = new UserTransaction();
			BeanUtils.copyProperties(transaction, response);
			return response;

		}).collect(Collectors.toList());

		TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setUserTransactions(userTransaction);
		transactionResponse.setSizeOfList(transactions.size());

		return transactionResponse;
	}

}
