package com.hcl.bank.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

public class UserDTO {

	@Size(min = 5,message ="minimum five characters required")
	private String userName;

	@Size(min = 7,message ="minimum seven characters required")
	private String password;

	@Email(message = "email required")
	private String email;
	
	@Size(min = 5,message ="minimum five characters required")
	private String address;
	
	@Size(min = 8,message ="minimum eight characters required")
	private String accountType;
	
	@NotNull
	private Long balanceAmount;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Long getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(Long balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	
	


}
