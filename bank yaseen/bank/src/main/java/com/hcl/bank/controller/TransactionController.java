package com.hcl.bank.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.bank.dto.TransactionDTO;
import com.hcl.bank.exception.FromAccountNotFoundException;
import com.hcl.bank.exception.InsufficientFundException;
import com.hcl.bank.exception.ToAccoountNotFoundException;
import com.hcl.bank.exception.TransactionNotFoundException;
import com.hcl.bank.exception.UserNotFoundExcption;
import com.hcl.bank.response.TransactionResponse;
import com.hcl.bank.service.TransactionService;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

	@Autowired
	private TransactionService transactionService;

	private static Logger logger = LoggerFactory.getLogger(UserController.class);

	/**
	 * 
	 * @param transactionDTO
	 * @return fund transfer successful message
	 * @throws FromAccountNotFoundException
	 * @throws ToAccoountNotFoundException
	 * @throws InsufficientFundException 
	 */

	@PostMapping("")
	public ResponseEntity<String> fundTransfer(@Valid @RequestBody(required = true) TransactionDTO transactionDTO)
			throws FromAccountNotFoundException, ToAccoountNotFoundException, InsufficientFundException {

		logger.info("Fund transfer");

		String fundTransfer = transactionService.fundTransfer(transactionDTO);

		return new ResponseEntity<String>(fundTransfer, HttpStatus.OK);

	}

	/**
	 * 
	 * @param userId
	 * @param pageNumber
	 * @param pageSize
	 * @return returns user transaction details
	 * @throws UserNotFoundExcption
	 * @throws TransactionNotFoundException
	 */
	@GetMapping("/{userId}")
	public ResponseEntity<TransactionResponse> transactionStatement(@PathVariable("userId") Long userId,
			@RequestParam Integer pageNumber, Integer pageSize)
			throws UserNotFoundExcption, TransactionNotFoundException {

		logger.info("fetching user transactions");

		TransactionResponse fundTransfer = transactionService.transactionStatement(userId, pageNumber, pageSize);

		return new ResponseEntity<TransactionResponse>(fundTransfer, HttpStatus.OK);

	}
}
