package com.hcl.bank.exception;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

	/*
	 * protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object
	 * body, HttpHeaders headers, HttpStatus status, WebRequest request) {
	 * 
	 * if (ex instanceof MethodArgumentTypeMismatchException) {
	 * MethodArgumentTypeMismatchException exception =
	 * (MethodArgumentTypeMismatchException) ex;
	 * 
	 * List<String> errorList =
	 * exception.getBindingResult().getFeildErrors().stream() .map(feildError ->
	 * feildError.getDefaultMessage()).collect(Collectors.toList());
	 * 
	 * ValidationException errorDetails = new
	 * ValidationException("once check the validation", 500, errorList); return
	 * super.handleExceptionInternal(ex, body, headers, status, request);
	 * 
	 * }
	 * 
	 * return super.handleExceptionInternal(ex, body, headers, status, request);
	 * 
	 * }
	 */

}
