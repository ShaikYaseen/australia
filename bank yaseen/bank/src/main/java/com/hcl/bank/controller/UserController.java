package com.hcl.bank.controller;

import java.util.List;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hcl.bank.dto.UserDTO;
import com.hcl.bank.exception.InvalidAccountException;
import com.hcl.bank.exception.InvalidUserException;
import com.hcl.bank.exception.MinimumBalanceRequiredException;
import com.hcl.bank.exception.UserNotFoundExcption;
import com.hcl.bank.model.User;
import com.hcl.bank.response.UserAccount;
import com.hcl.bank.response.UserResponse;
import com.hcl.bank.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService userService;

	private static Logger logger = LoggerFactory.getLogger(UserController.class);

	/**
	 * 
	 * @param userDTO
	 * @return successful registration message and user details
	 * @throws InvalidAccountException
	 * @throws InvalidUserException
	 * @throws MinimumBalanceRequiredException
	 */
	@PostMapping("")
	public ResponseEntity<UserResponse> saveUser(@Valid @RequestBody(required = true) UserDTO userDTO)
			throws InvalidAccountException, InvalidUserException, MinimumBalanceRequiredException {
		logger.info("saving user");

		if (userDTO.getBalanceAmount() <= 5000) {
			throw new MinimumBalanceRequiredException("minimum 5000 amount required");
		}
		User user = userService.saveUser(userDTO);
		UserResponse userResponse = new UserResponse();
		userResponse.setMessage("successfully registered");
		userResponse.setUser(user);

		return new ResponseEntity<UserResponse>(userResponse, HttpStatus.OK);
	}

	/**
	 * 
	 * @param userId
	 * @return user account details
	 * @throws UserNotFoundExcption
	 */

	@GetMapping("/{userId}")
	public ResponseEntity<List<UserAccount>> getUserAccounts(@PathVariable("userId") Long userId)
			throws UserNotFoundExcption {
		logger.info("fetching user details");

		List<UserAccount> userAccounts = userService.getUserAccounts(userId);

		return new ResponseEntity<List<UserAccount>>(userAccounts, HttpStatus.OK);
	}

	/**
	 * 
	 * @param userId
	 * @return delete user and success message
	 * @throws UserNotFoundExcption
	 */

	@DeleteMapping("/{userId}")
	public ResponseEntity<String> deleteUser(@PathVariable("userId") Long userId) throws UserNotFoundExcption {
		logger.info("delete user");

		String userRegister = userService.deleteUser(userId);

		return new ResponseEntity<String>(userRegister, HttpStatus.OK);
	}

}
