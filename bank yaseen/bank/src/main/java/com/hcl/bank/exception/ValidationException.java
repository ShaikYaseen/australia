package com.hcl.bank.exception;

import java.util.List;

public class ValidationException {

	private String message;

	private int status;

	private List<String> details;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<String> getDetails() {
		return details;
	}

	public void setDetails(List<String> details) {
		this.details = details;
	}

	public ValidationException(String message, int status, List<String> details) {
		super();
		this.message = message;
		this.status = status;
		this.details = details;
	}
	

}
