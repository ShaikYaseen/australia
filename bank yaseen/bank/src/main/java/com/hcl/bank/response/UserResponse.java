package com.hcl.bank.response;

import com.hcl.bank.model.User;

public class UserResponse {

	private String message;
	
	private User user;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
