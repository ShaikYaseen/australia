package com.demobank.accountmanagement.repositorie;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demobank.accountmanagement.model.Customer;



@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

	Optional<Customer> findByCustomerEmailIdAndPassword(String emailId, String password);

	Customer findByCustomerId(Long customerId);
	
	

	

}
