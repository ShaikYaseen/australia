package com.demobank.accountmanagement.restcontroller;

import javax.naming.InsufficientResourcesException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.demobank.accountmanagement.dto.FundTransferdto;
import com.demobank.accountmanagement.exception.AccountNotFoundException;
import com.demobank.accountmanagement.response.TransactionResponse;
import com.demobank.accountmanagement.service.FundService;
import com.demobank.accountmanagement.service.TransactionService;

@RestController
@RequestMapping("/bank")
public class FundController {

	@Autowired
	private FundService fundService;

	@Autowired
	private TransactionService transactionService;

	@PostMapping("")
	public ResponseEntity<String> fundTransfer(@RequestBody FundTransferdto fundTransferdto)
			throws InsufficientResourcesException, AccountNotFoundException {

		String fundTransferMessage = fundService.fundTransfer(fundTransferdto);

		return new ResponseEntity<String>(fundTransferMessage, HttpStatus.OK);

	}

	@GetMapping("/{accountId}")
	public ResponseEntity<TransactionResponse> getTransactionsStatement(@PathVariable Long accountId,
			@RequestParam Integer pageSize, @RequestParam Integer pageNumber) {

		TransactionResponse transactionResponse = transactionService.getTransactionsStatement(accountId, pageSize,
				pageNumber);

		return new ResponseEntity<TransactionResponse>(transactionResponse, HttpStatus.OK);

	}

}
