package com.demobank.accountmanagement.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.jboss.logging.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demobank.accountmanagement.dto.BenificiaryDTO;
import com.demobank.accountmanagement.dto.BenificiaryRequest;
import com.demobank.accountmanagement.dto.CustomerDto;
import com.demobank.accountmanagement.exception.AccountNotFoundException;
import com.demobank.accountmanagement.exception.BenificiaryNotFoundException;
import com.demobank.accountmanagement.exception.CustomerNotLoggedInException;
import com.demobank.accountmanagement.exception.UserNotFoundException;
import com.demobank.accountmanagement.model.Account;
import com.demobank.accountmanagement.model.Benificiary;
import com.demobank.accountmanagement.model.Customer;
import com.demobank.accountmanagement.repositorie.AccountRepository;
import com.demobank.accountmanagement.repositorie.BenificiaryRepository;
import com.demobank.accountmanagement.repositorie.CustomerRepository;

@Service
public class BenificiaryService {

	@Autowired
	BenificiaryRepository benificiaryRepository;

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	CustomerService customerService;

	@Autowired
	CustomerRepository customerRepository;

	Logger log = Logger.getLogger(BenificiaryService.class);

	public void addBenificiary(BenificiaryRequest benificiaryRequest)
			throws UserNotFoundException, AccountNotFoundException, BeansException, CustomerNotLoggedInException {

		if (customerService.checkLoggingStatus(benificiaryRequest.getCustomerId())) {

			Account account = accountRepository.findByAccountNumber(benificiaryRequest.getAccountNumber());

			if (account == null) {
				throw new AccountNotFoundException();
			}

			Benificiary benificiary = new Benificiary();
			benificiary.setBenificiaryAccountNumber(benificiaryRequest.getAccountNumber());
			benificiary.setBenificiaryName(benificiaryRequest.getName());

			//CustomerDto customerDto = customerService.findCustomerByCustomerId(benificiaryRequest.getCustomerId());

			Customer customer = customerRepository.findByCustomerId(benificiaryRequest.getCustomerId());
			List<Benificiary> benificiaries = new ArrayList<Benificiary>();
			benificiaries.add(benificiary);

			customer.setBenificiaries(benificiaries);
			
			
			/*
			 * BeanUtils.copyProperties(customerDto, customer);
			 * benificiary.setCustomer(customer);
			 */
			
			customerRepository.save(customer);
			log.info("Benificiary added successfully.");

		}

		throw new CustomerNotLoggedInException();

	}

	public void updatedBenificiary(BenificiaryDTO benificiaryRequest)
			throws BenificiaryNotFoundException, UserNotFoundException {

		Optional<Benificiary> benificiary = benificiaryRepository
				.findByBenificiaryId(benificiaryRequest.getBenificiaryId());
		if (benificiary.isPresent()) {

			benificiary.get().setBenificiaryAccountNumber(benificiaryRequest.getBenificiaryAccountNumber());
			benificiary.get().setBenificiaryName(benificiaryRequest.getBenificiaryName());

			CustomerDto customerDto = customerService.findCustomerByCustomerId(benificiaryRequest.getCustomerId());

			Customer customer = new Customer();

			BeanUtils.copyProperties(customerDto, customer);
			benificiary.get().setCustomer(customer);

			benificiaryRepository.save(benificiary.get());
			log.info("Benificiary added successfully.");
		} else {
			throw new BenificiaryNotFoundException();
		}

	}

	public void deleteBenificiary(Long benificiaryId) throws BenificiaryNotFoundException {

		Optional<Benificiary> benificiary = benificiaryRepository.findByBenificiaryId(benificiaryId);
		if (benificiary.isPresent()) {
			benificiaryRepository.deleteById(benificiary.get().getBenificiaryId());
		} else {
			throw new BenificiaryNotFoundException();
		}

	}

	public BenificiaryDTO getBenificiaryByBenificiaryId(Long benificiaryId) throws BenificiaryNotFoundException {
		Optional<Benificiary> benificiary = benificiaryRepository.findByBenificiaryId(benificiaryId);
		if (benificiary.isPresent()) {
			BenificiaryDTO benificiaryDto = new BenificiaryDTO();
			benificiaryDto.setBenificiaryId(benificiary.get().getBenificiaryId());
			benificiaryDto.setBenificiaryAccountNumber(benificiary.get().getBenificiaryAccountNumber());
			benificiaryDto.setBenificiaryName(benificiary.get().getBenificiaryName());
			benificiaryDto.setCustomerId(benificiary.get().getCustomer().getCustomerId());
			return benificiaryDto;
		} else {
			throw new BenificiaryNotFoundException();
		}
	}

	/*
	 * @Override public List<BenificiaryDTO> getAllBenificiaryByCustomerId(Long
	 * customerId) throws BenificiaryNotFoundException { List<Benificiary>
	 * benificiary = benificiaryRepository.findByCustomer(customerId));
	 * if(CollectionUtils.isEmpty(benificiary)) {
	 * 
	 * }
	 * 
	 * 
	 * return benificiary; }
	 */
}
