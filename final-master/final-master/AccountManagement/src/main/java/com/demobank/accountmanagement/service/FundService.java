package com.demobank.accountmanagement.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.naming.InsufficientResourcesException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.demobank.accountmanagement.dto.FundTransferdto;
import com.demobank.accountmanagement.exception.AccountNotFoundException;
import com.demobank.accountmanagement.model.Account;
import com.demobank.accountmanagement.model.Transaction;
import com.demobank.accountmanagement.repositorie.AccountRepository;
import com.demobank.accountmanagement.repositorie.FundTransferRepository;
import com.demobank.accountmanagement.repositorie.TransactionRepository;

@Service
public class FundService {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private TransactionRepository transactionRepository;

	@Transactional
	public String fundTransfer(FundTransferdto fundTransferdto)
			throws InsufficientResourcesException, AccountNotFoundException {

		Optional<Account> fromAccountCheck = accountRepository.findByAccountNumber(fundTransferdto.getFromAccount());

		if (!fromAccountCheck.isPresent()) {

			throw new AccountNotFoundException("account not found exception");
		}

		Account fromAccount = fromAccountCheck.get();

		Optional<Account> toAccountCheck = accountRepository.findByAccountNumber(fundTransferdto.getToAccount());
		Account toAccount = toAccountCheck.get();

		if (!toAccountCheck.isPresent()) {

			throw new AccountNotFoundException("Account not found exception");
		}

		if (fromAccount.getAvailableBalance() < fundTransferdto.getAmount()) {
			throw new InsufficientResourcesException("Insufficient fund exception ");
		}

		fromAccount.setAvailableBalance(fromAccount.getAvailableBalance() - fundTransferdto.getAmount());

		toAccount.setAvailableBalance(toAccount.getAvailableBalance() + fundTransferdto.getAmount());

		Transaction fromTransaction = new Transaction();

		fromTransaction.setAmount(fundTransferdto.getAmount());
		fromTransaction.setTransactionDateTime(new Date());
		fromTransaction.setTransactionStatus("success");
		fromTransaction.setTransactionDescription(fundTransferdto.getDescription());
		fromTransaction.setTransactionType("debited");
		List<Transaction> fromTransactionList = new ArrayList<Transaction>();
		fromTransactionList.add(fromTransaction);

		fromAccount.setTransaction(fromTransactionList);
		accountRepository.save(fromAccount);

		Transaction toTransaction = new Transaction();
		toTransaction.setAmount(fundTransferdto.getAmount());
		toTransaction.setTransactionDateTime(new Date());
		toTransaction.setTransactionStatus("success");
		toTransaction.setTransactionDescription(fundTransferdto.getDescription());
		toTransaction.setTransactionType("credited");

		List<Transaction> toTransactionList = new ArrayList<Transaction>();
		toTransactionList.add(toTransaction);

		toAccount.setTransaction(toTransactionList);
		accountRepository.save(toAccount);
		return "Transaction successful";

	}

}
