package com.demobank.accountmanagement.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.demobank.accountmanagement.dto.Transactiondto;
import com.demobank.accountmanagement.model.Transaction;
import com.demobank.accountmanagement.repositorie.TransactionRepository;
import com.demobank.accountmanagement.response.TransactionResponse;

@Service
public class TransactionService {
	
	@Autowired
	private TransactionRepository transactionRepository;

	public List<Transaction> getAllTransaction(Long accountId) {
		
		List<Transaction> transactions = transactionRepository.findByAccountAccountId(accountId);
		return transactions;
	}

	public TransactionResponse getTransactionsStatement(Long accountId, Integer pageSize, Integer pageNumber) {

		PageRequest pageable = PageRequest.of(pageSize, pageNumber);

		List<Transaction> transactions = transactionRepository.findByAccountId(accountId, pageable);

		List<Transactiondto> transactiondtos = transactions.stream().map(transaction -> {

			Transactiondto transactiondto = new Transactiondto();
			BeanUtils.copyProperties(transaction, transactiondto);
			return transactiondto;

		}).collect(Collectors.toList());

		TransactionResponse transactionResponse = new TransactionResponse();
		transactionResponse.setFundTransfer(transactiondtos);
		transactionResponse.setSizeOfList(transactiondtos.size());

		return transactionResponse;
	}

}
