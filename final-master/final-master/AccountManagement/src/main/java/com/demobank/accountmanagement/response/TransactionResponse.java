package com.demobank.accountmanagement.response;

import java.util.List;

import com.demobank.accountmanagement.dto.Transactiondto;

public class TransactionResponse {

	private Integer sizeOfList;

	private List<Transactiondto> fundTransfer;

	public Integer getSizeOfList() {
		return sizeOfList;
	}

	public void setSizeOfList(Integer i) {
		this.sizeOfList = i;
	}

	public List<Transactiondto> getFundTransfer() {
		return fundTransfer;
	}

	public void setFundTransfer(List<Transactiondto> fundTransfer) {
		this.fundTransfer = fundTransfer;
	}

}
