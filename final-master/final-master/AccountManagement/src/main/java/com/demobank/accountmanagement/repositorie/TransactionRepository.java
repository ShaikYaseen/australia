package com.demobank.accountmanagement.repositorie;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demobank.accountmanagement.model.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
	public List<Transaction> findByAccountAccountId(Long accountid);

	public List<Transaction> findByAccountId(Long accountId, PageRequest pageable);
}
