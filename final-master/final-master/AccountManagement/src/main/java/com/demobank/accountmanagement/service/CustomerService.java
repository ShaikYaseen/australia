package com.demobank.accountmanagement.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demobank.accountmanagement.dto.CustomerDto;
import com.demobank.accountmanagement.exception.CustomerNotLoggedInException;
import com.demobank.accountmanagement.exception.UserNotFoundException;
import com.demobank.accountmanagement.model.Customer;
import com.demobank.accountmanagement.repositorie.CustomerRepository;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	public String customerLogin(String emailId, String password) throws UserNotFoundException {

		Optional<Customer> customer = customerRepository.findByCustomerEmailIdAndPassword(emailId, password);

		if (!customer.isPresent()) {
			throw new UserNotFoundException();
		}

		customer.get().setIsLoggedIn(true);

		customerRepository.save(customer.get());

		return "login successfull";
	}

	public Boolean checkLoggingStatus(Long customerId) throws CustomerNotLoggedInException {
		Optional<Customer> customer = customerRepository.findByCustomerId(customerId);
		if (!customer.isPresent()) {
			throw new UserNotFoundException();
		} else {
			return customer.get().getIsLoggedIn();
		}
	}

	public CustomerDto findCustomerByCustomerId(Long customerId) throws UserNotFoundException {

		Optional<Customer> customer = customerRepository.findByCustomerId(customerId);
		if (!customer.isPresent()) {
			throw new UserNotFoundException();
		}
		CustomerDto customerDto = new CustomerDto();
		BeanUtils.copyProperties(customer, customerDto);
		return customerDto;
	}

}
