package com.demobank.accountmanagement.restcontroller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.demobank.accountmanagement.dto.BenificiaryDTO;
import com.demobank.accountmanagement.dto.BenificiaryRequest;
import com.demobank.accountmanagement.exception.AccountNotFoundException;
import com.demobank.accountmanagement.exception.BenificiaryNotFoundException;
import com.demobank.accountmanagement.exception.UserNotFoundException;
import com.demobank.accountmanagement.service.BenificiaryService;

@RestController
@RequestMapping("/benificiaries")
public class BenificiaryRestController {

	@Autowired
	BenificiaryService benificiaryService;

	@PostMapping("")
	public ResponseEntity<Object> addBenificiary(@Valid @RequestBody BenificiaryRequest benificiaryRequest) 
			throws UserNotFoundException, AccountNotFoundException {

		benificiaryService.addBenificiary(benificiaryRequest);
		return new ResponseEntity<>("Benificiary added successfully.", HttpStatus.CREATED);
		
	}

	@PutMapping("")
	public ResponseEntity<Object> updateBenificiary(@RequestBody BenificiaryDTO benificiaryRequest) {

		benificiaryService.updatedBenificiary(benificiaryRequest);
		return new ResponseEntity<>("Benificiary updated successfully.", HttpStatus.OK);
	}

	@DeleteMapping("/{benificiaryId}")
	public ResponseEntity<Object> deleteBenificiary(@PathVariable("benificiaryId") Long benificiaryId)
			throws BenificiaryNotFoundException {
		benificiaryService.deleteBenificiary(benificiaryId);
		return new ResponseEntity<>("Customer Deleted Successfully", HttpStatus.NO_CONTENT);
	}

	@GetMapping("/{benificiaryId}")
	public ResponseEntity<Object> getBenificiaryByBenificiaryId(@PathVariable("benificiaryId") Long benificiaryId)
			throws BenificiaryNotFoundException {
		BenificiaryDTO benificiaryDTO = benificiaryService.getBenificiaryByBenificiaryId(benificiaryId);
		return new ResponseEntity<>(benificiaryDTO, HttpStatus.OK);
	}

}
