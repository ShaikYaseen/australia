package com.tmsdemo.tradingmanagementsystem.exception;

/**
 * Exception to be throw when the stocks are not available with the stock name
 * 
 * @author ShaikYaseen
 *
 */
public class UserNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	private Integer eRROR_CODE = 669;

	public UserNotFoundException(String message, Integer eRROR_CODE) {
		super(message);
		this.eRROR_CODE = eRROR_CODE;
	}

	public Integer getErrorCode() {
		return this.eRROR_CODE;
	}

}
