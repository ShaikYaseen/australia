package com.tmsdemo.tradingmanagementsystem.exception;

/**
 * GlobalExceptionHandler class is used to handle all the exception classes
 * @author ShaikYaseen
 */
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<Object> userNotFoundException(UserNotFoundException userNotFoundException) {

		ExceptionResponseDTO exceptionResponseDTO = new ExceptionResponseDTO(userNotFoundException.getMessage(),
				userNotFoundException.getErrorCode());

		return new ResponseEntity<>(exceptionResponseDTO, HttpStatus.NOT_FOUND);

	}

}
