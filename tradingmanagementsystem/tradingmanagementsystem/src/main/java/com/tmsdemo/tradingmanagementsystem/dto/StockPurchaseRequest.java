package com.tmsdemo.tradingmanagementsystem.dto;

/**
 * StockPurchaseRequest class is used to send the stock purchase request
 * @author ShaikYaseen
 */
import lombok.Setter;

import javax.validation.constraints.NotNull;

import lombok.Getter;

@Getter
@Setter
public class StockPurchaseRequest {
	@NotNull
	private Long stockId;
	@NotNull
	private Integer quantity;
}
