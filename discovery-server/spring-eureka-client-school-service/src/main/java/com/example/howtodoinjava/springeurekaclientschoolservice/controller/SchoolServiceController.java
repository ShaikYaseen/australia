package com.example.howtodoinjava.springeurekaclientschoolservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SchoolServiceController {

	/*
	 * @Autowired RestTemplate restTemplate;
	 * 
	 * @RequestMapping(value = "/getSchoolDetails/{schoolname}", method =
	 * RequestMethod.GET) public String getStudents(@PathVariable String schoolname)
	 * { System.out.println("Getting School details for " + schoolname); String
	 * response = restTemplate.exchange(
	 * "http://student-service/getStudentDetailsForSchool/{schoolname}",
	 * HttpMethod.GET, null, new ParameterizedTypeReference<String>() { },
	 * schoolname).getBody();
	 * 
	 * System.out.println("Response Received as " + response);
	 * 
	 * return "School Name -  " + schoolname + " \n Student Details " + response; }
	 * 
	 * @Bean
	 * 
	 * @LoadBalanced public RestTemplate restTemplate() { return new RestTemplate();
	 * }
	 */

	@Autowired
	private StudentForiegn studentFeign;

	@GetMapping("/getSchoolDetails")
	public String getStudents() {
		String response = studentFeign.getStudents();

		return "School Name  Student Details " + response;
	}

}
