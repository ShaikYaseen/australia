package com.example.howtodoinjava.springeurekaclientschoolservice.controller;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "student-service")
@RibbonClient(name = "student-service")
public interface StudentForiegn {

	@GetMapping("/getStudentDetailsForSchool")
	public String getStudents();

}
